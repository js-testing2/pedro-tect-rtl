import Counter from "./Counter";
// fireEvent - simulates user events
import { render, fireEvent } from "@testing-library/react";
// Each test file should represent a test for a specific component
// Describe - describing all the different ways the component should work
// Render function provides us different methods for querying - getBy.., query..
describe(Counter, () => {
  // it - describe the test for the component
  // callback - describes the action that will occur in the test
  // render - serves to render a fake version of the component
  it("Counter displays correct initial count", () => {
    // getByTestId - serves for elements that are dynamically changing
    const { getByTestId } = render(<Counter initialCount={0} />);
    const countValue = Number(getByTestId("count").textContent);
    // Expect - what we expect to happen
    expect(countValue).toEqual(0);
  });

  it("Count should increment by 1 if the increment button is clicked", () => {
    // getByRole - what the element is
    const { getByTestId, getByRole } = render(<Counter initialCount={0} />);
    // get the button by the name - Gets button with Increment name inside
    const incrementBtn = getByRole("button", { name: "Increment" });
    const countValue1 = Number(getByTestId("count").textContent);
    expect(countValue1).toEqual(0);
    fireEvent.click(incrementBtn);
    const countValue2 = Number(getByTestId("count").textContent);
    expect(countValue2).toEqual(1);
    // We can test to check if the counter value was 0 before it increments
    // const countValue = Number(getByTestId("count").textContent);
    // expect(countValue).toEqual(1);
  });

  it("Count should decrement by 1 if the decrement button is clicked", () => {
    // getByRole - what the element is
    const { getByTestId, getByRole } = render(<Counter initialCount={0} />);
    const decrementBtn = getByRole("button", { name: "Decrement" });
    expect(Number(getByTestId("count").textContent)).toEqual(0);
    fireEvent.click(decrementBtn);
    expect(Number(getByTestId("count").textContent)).toEqual(-1);
  });

  it("Count should be 0 if the restart button is clicked", () => {
    // getByRole - what the element is
    const { getByTestId, getByRole } = render(<Counter initialCount={50} />);
    const restarttBtn = getByRole("button", { name: "Restart" });
    expect(Number(getByTestId("count").textContent)).toEqual(50);
    fireEvent.click(restarttBtn);
    expect(Number(getByTestId("count").textContent)).toEqual(0);
  });

  it("Count should invert signs if the switch signs button is clicked", () => {
    // getByRole - what the element is
    const { getByTestId, getByRole } = render(<Counter initialCount={50} />);
    const switchBtn = getByRole("button", { name: "Switch Signs" });
    expect(Number(getByTestId("count").textContent)).toEqual(50);
    fireEvent.click(switchBtn);
    expect(Number(getByTestId("count").textContent)).toEqual(-50);
  });
});
